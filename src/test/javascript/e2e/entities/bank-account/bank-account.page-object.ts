import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class BankAccountComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-bank-account div table .btn-danger'));
  title = element.all(by.css('jhi-bank-account div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BankAccountUpdatePage {
  pageTitle = element(by.id('jhi-bank-account-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  ibanInput = element(by.id('field_iban'));
  bicInput = element(by.id('field_bic'));
  balanceInput = element(by.id('field_balance'));
  useridSelect = element(by.id('field_userid'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIbanInput(iban) {
    await this.ibanInput.sendKeys(iban);
  }

  async getIbanInput() {
    return await this.ibanInput.getAttribute('value');
  }

  async setBicInput(bic) {
    await this.bicInput.sendKeys(bic);
  }

  async getBicInput() {
    return await this.bicInput.getAttribute('value');
  }

  async setBalanceInput(balance) {
    await this.balanceInput.sendKeys(balance);
  }

  async getBalanceInput() {
    return await this.balanceInput.getAttribute('value');
  }

  async useridSelectLastOption(timeout?: number) {
    await this.useridSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async useridSelectOption(option) {
    await this.useridSelect.sendKeys(option);
  }

  getUseridSelect(): ElementFinder {
    return this.useridSelect;
  }

  async getUseridSelectedOption() {
    return await this.useridSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BankAccountDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-bankAccount-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-bankAccount'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
