import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class TransactionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-transaction div table .btn-danger'));
  title = element.all(by.css('jhi-transaction div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class TransactionUpdatePage {
  pageTitle = element(by.id('jhi-transaction-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  valueInput = element(by.id('field_value'));
  verwendungszweckInput = element(by.id('field_verwendungszweck'));
  zahlungsreferenzInput = element(by.id('field_zahlungsreferenz'));
  senderSelect = element(by.id('field_sender'));
  receiverSelect = element(by.id('field_receiver'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setValueInput(value) {
    await this.valueInput.sendKeys(value);
  }

  async getValueInput() {
    return await this.valueInput.getAttribute('value');
  }

  async setVerwendungszweckInput(verwendungszweck) {
    await this.verwendungszweckInput.sendKeys(verwendungszweck);
  }

  async getVerwendungszweckInput() {
    return await this.verwendungszweckInput.getAttribute('value');
  }

  async setZahlungsreferenzInput(zahlungsreferenz) {
    await this.zahlungsreferenzInput.sendKeys(zahlungsreferenz);
  }

  async getZahlungsreferenzInput() {
    return await this.zahlungsreferenzInput.getAttribute('value');
  }

  async senderSelectLastOption(timeout?: number) {
    await this.senderSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async senderSelectOption(option) {
    await this.senderSelect.sendKeys(option);
  }

  getSenderSelect(): ElementFinder {
    return this.senderSelect;
  }

  async getSenderSelectedOption() {
    return await this.senderSelect.element(by.css('option:checked')).getText();
  }

  async receiverSelectLastOption(timeout?: number) {
    await this.receiverSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async receiverSelectOption(option) {
    await this.receiverSelect.sendKeys(option);
  }

  getReceiverSelect(): ElementFinder {
    return this.receiverSelect;
  }

  async getReceiverSelectedOption() {
    return await this.receiverSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class TransactionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-transaction-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-transaction'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
