/**
 * View Models used by Spring MVC REST controllers.
 */
package at.hakimst.ebanking.web.rest.vm;
