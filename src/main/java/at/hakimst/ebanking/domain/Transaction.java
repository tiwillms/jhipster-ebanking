package at.hakimst.ebanking.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Transaction.
 */
@Entity
@Table(name = "transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_value", nullable = false)
    private Double value;

    @NotNull
    @Column(name = "verwendungszweck", nullable = false)
    private String verwendungszweck;

    @Column(name = "zahlungsreferenz")
    private String zahlungsreferenz;

    @ManyToOne
    @JsonIgnoreProperties("transactions")
    private BankAccount sender;

    @ManyToOne
    @JsonIgnoreProperties("transactions")
    private BankAccount receiver;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValue() {
        return value;
    }

    public Transaction value(Double value) {
        this.value = value;
        return this;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getVerwendungszweck() {
        return verwendungszweck;
    }

    public Transaction verwendungszweck(String verwendungszweck) {
        this.verwendungszweck = verwendungszweck;
        return this;
    }

    public void setVerwendungszweck(String verwendungszweck) {
        this.verwendungszweck = verwendungszweck;
    }

    public String getZahlungsreferenz() {
        return zahlungsreferenz;
    }

    public Transaction zahlungsreferenz(String zahlungsreferenz) {
        this.zahlungsreferenz = zahlungsreferenz;
        return this;
    }

    public void setZahlungsreferenz(String zahlungsreferenz) {
        this.zahlungsreferenz = zahlungsreferenz;
    }

    public BankAccount getSender() {
        return sender;
    }

    public Transaction sender(BankAccount bankAccount) {
        this.sender = bankAccount;
        return this;
    }

    public void setSender(BankAccount bankAccount) {
        this.sender = bankAccount;
    }

    public BankAccount getReceiver() {
        return receiver;
    }

    public Transaction receiver(BankAccount bankAccount) {
        this.receiver = bankAccount;
        return this;
    }

    public void setReceiver(BankAccount bankAccount) {
        this.receiver = bankAccount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", value=" + getValue() +
            ", verwendungszweck='" + getVerwendungszweck() + "'" +
            ", zahlungsreferenz='" + getZahlungsreferenz() + "'" +
            "}";
    }
}
