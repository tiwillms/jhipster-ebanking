import { IBankAccount } from 'app/shared/model/bank-account.model';

export interface ITransaction {
  id?: number;
  value?: number;
  verwendungszweck?: string;
  zahlungsreferenz?: string;
  sender?: IBankAccount;
  receiver?: IBankAccount;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public value?: number,
    public verwendungszweck?: string,
    public zahlungsreferenz?: string,
    public sender?: IBankAccount,
    public receiver?: IBankAccount
  ) {}
}
