import { IUser } from 'app/core/user/user.model';

export interface IBankAccount {
  id?: number;
  iban?: string;
  bic?: string;
  balance?: number;
  userid?: IUser;
}

export class BankAccount implements IBankAccount {
  constructor(public id?: number, public iban?: string, public bic?: string, public balance?: number, public userid?: IUser) {}
}
