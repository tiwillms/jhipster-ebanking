import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EbankingSharedLibsModule, EbankingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [EbankingSharedLibsModule, EbankingSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [EbankingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EbankingSharedModule {
  static forRoot() {
    return {
      ngModule: EbankingSharedModule
    };
  }
}
